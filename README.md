# Computer Science Labs (2022-2021)
<br>
Each directory represents a singular Java lab that I did in the Advanced Computer Science class at Frisco High School.

The purpose of these labs may be ambiguous, but hopefully the names will help.

This repository is an *archive* of my work rather than to present a project that I have done. 